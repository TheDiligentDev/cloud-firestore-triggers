const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

const db = admin.firestore();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.onUserCreate = functions.firestore.document('users/{userId}').onCreate(async (snap, context) => {
    const values = snap.data();

    // send email
    await db.collection('logging').add({ description: `Email was sent to user with username:${values.username}` });
})

exports.onUserUpdate = functions.firestore.document('users/{userId}').onUpdate(async (snap, context) => {
    const newValues = snap.after.data();

    const previousValues = snap.before.data();

    if (newValues.username !== previousValues.username) {
        const snapshot = await db.collection('reviews').where('username', '==', previousValues.username).get();

        let updatePromises = [];
        snapshot.forEach(doc => {
            updatePromises.push(db.collection('reviews').doc(doc.id).update({ username: newValues.username }));
        });

        await Promise.all(updatePromises);
    }
})

exports.onPostDelete = functions.firestore.document('posts/{postId}').onDelete(async (snap, context) => {
    const deletedPost = snap.data();

    let deletePromises = [];
    const bucket = admin.storage().bucket();

    deletedPost.images.forEach(image => {
        deletePromises.push(bucket.file(image).delete());
    });

    await Promise.all(deletePromises);
});